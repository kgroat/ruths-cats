
import { promisify } from 'util'
import * as fs from 'fs'

const readFile = promisify(fs.readFile)

interface Template<T> {
  (values: T): string
}

export const buildTemplate = <T>(content: string): Template<T> => {
  content = content.replace(/`/g, '\\`')
  content = content.replace(/\$json\(([^)]+)\)/g, (_, name) => {
    return `\${JSON.stringify(${name})}`
  })

  return (values: T) => {
    const consts = Object.keys(values).map(key => (
      `const ${key} = values['${key}'];`
    )).join('\n')

    // tslint:disable-next-line:no-eval
    return eval(`(() => {
      ${consts}
      return \`${content}\`.replace(/\\\$/g, '$')
    })()`)
  }
}

const fileContentMap = new Map<string, Promise<Template<any>>>()

export async function templateFromFile<T> (filename: string): Promise<Template<T>> {
  if (!fileContentMap.has(filename)) {
    fileContentMap.set(filename, (async () => {
      const content = await readFile(filename)
      return buildTemplate(content.toString())
    })())
  }

  return fileContentMap.get(filename)!
}
