
import { promisify } from 'util'
import * as fs from 'fs'
import * as path from 'path'
import * as express from 'express'

import { templateFromFile } from './template'

const PORT = process.env.PORT || 3000
const app = express()

const readdir = promisify(fs.readdir)

const STATIC_DIR = path.join(__dirname, '../static')

async function readFiles (directory: string) {
  const files = await readdir(path.join(STATIC_DIR, directory))
  return files.map(file => `${directory}/${file}`)
}

const imagesPromise = readFiles('images')
const soundsPromise = readFiles('sounds')
const backdropsPromise = readFiles('backdrops')

interface TemplateParams {
  images: string[]
  sounds: string[]
  backdrops: string[]
}

app.get('/', async (req, res) => {
  const [
    template,
    images,
    sounds,
    backdrops,
  ] = await Promise.all([
    templateFromFile<TemplateParams>(path.join(__dirname, './index.html')),
    imagesPromise,
    soundsPromise,
    backdropsPromise,
  ])

  res.send(template({
    images,
    sounds,
    backdrops,
  }))
})

app.use(express.static(path.join(__dirname, '../static')))

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`)
})
