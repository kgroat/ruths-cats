# Ruth's Cats
## A simple page dedicated to cats
[See it live](http://ruths-cats.herokuapp.com/)

### Running locally
1. Clone the repository
1. `npm i`
1. `npm run dev`
1. Open http://localhost:3000 in your browser

### Adding new cats, backdrops, and sounds
Inside the `static` directory, you will find three subdirectories.
* backdrops -- Place large images here that will be used as backdrops on the page
* images -- Place pictures of cats with transparent backgrounds here as options of cats that can be used
* sounds -- place mp3 files here that will be chosen randomly to be played whenever the user clicks the page
